import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css']
})
export class CareersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items = [
    {
      title:'Graphics Designer II',
      position: 'Full Time Position',
      date: 'Monday, July 20, 2020'
    },
    {
      title:'Graphics Designer II',
      position: 'Full Time Position',
      date: 'Monday, July 20, 2020'
    },
    {
      title:'Graphics Designer II',
      position: 'Full Time Position',
      date: 'Monday, July 20, 2020'
    },
  ]

}
