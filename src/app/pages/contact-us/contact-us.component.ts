import { Component, OnInit } from '@angular/core';
import emailjs, { EmailJSResponseStatus } from 'emailjs-com';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { emailValidator } from 'src/app/utils/app-validators';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  contactUsError: any;


  constructor(private _snackBar: MatSnackBar, public formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.contactUsForm = this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, emailValidator])],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(11)])],
      description: ['', Validators.compose([Validators.required])],
    });
  }

  openSnackBar(message: 'Email Sent!', action: 'OK') {
    this._snackBar.open(message, action, {
      duration: 3000,
      panelClass: ['my-snack-bar']
    });
  }
  errorSnackBar(message: 'Please Fill the Form!', action: 'OK') {
    this._snackBar.open(message, action, {
      duration: 3000,
      panelClass: ['my-snack-bar']
    });
  }

  public sendEmail(e: Event) {
    if (this.contactUsForm.controls.firstName.errors?.required ||
      this.contactUsForm.controls.lastName.errors?.required ||
      this.contactUsForm.controls.email.errors?.required ||
      this.contactUsForm.controls.phoneNumber.errors?.required ||
      this.contactUsForm.controls.description.errors?.required ||
      this.contactUsForm.controls.email.hasError('invalidEmail')) {
      this.errorSnackBar('Please Fill the Form!', 'OK');
    }
    else {
      e.preventDefault();
      emailjs.sendForm('gmail', 'template_VTevB6kq', e.target as HTMLFormElement, 'user_AmeM3wRcHW05uBzECjcoI')
        .then((result: EmailJSResponseStatus) => {
          console.log(result.text);
          this.openSnackBar('Email Sent!', 'OK');
        }, (error) => {
          console.log(error.text);
        });
    }
  }
}
