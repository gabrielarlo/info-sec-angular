import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items =[
    {
      title: 'Mission',
      description: 'W-Blue Online Trading & Services is passionate about delivering service excellence and great customer experience. We create innovative solutions, which deliver compelling outsourcing services, creating value for shareholders, clients and employees, whilst maintaining a broad commitment to the environment and society.'
    },
    {
      title: 'Vision',
      description: 'We aim to drive knowledge, innovation, technology and professional ethics to further improve the business environment and shape the future of knowledge-based economy.'
    }
  ]

}
