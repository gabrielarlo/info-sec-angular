import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.css']
})
export class OurServicesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items = [
    {
      link: './mobile-application-development',
      img: 'assets/images/web-programming.png',
      alt: 'mobile application development',
      title: 'Mobile Application Development',
      description: 'We develop & customize mobile applications derived from customer requirements.'
    },
    {
      link: './w-express',
      img: 'assets/images/food-delivery.png',
      alt: 'w-express',
      title: 'W-Express',
      description: 'Unique delivery system app incorporated with our online shopping marketplace that makes every delivery hassle free.'
    },
    {
      link: './w-mall',
      img: 'assets/images/market.png',
      alt: 'w-mall (online marketplace)',
      title: 'W-Mall (Online Marketplace)',
      description: 'An online shopping platform that is easy to use equipped with a real time delivery system and order tracking system and safety features.'
    },
    {
      link: './w-live',
      img: 'assets/images/interview.png',
      alt: 'w-live',
      title: 'W-Live',
      description: 'A social app for meeting new people around Doha and new social networking site to return social media to a concept of friends, families and followers.'
    },
    {
      link: './information-security',
      img: 'assets/images/shield.png',
      alt: 'info sec',
      title: 'InfoSec (Information Security)',
      description: 'Preventing unauthorized access, use, disclosure, disruption, modification, inspection, recording or destruction of information of a company.'
    },
    {
      link: './w-blue-hr-system',
      img: 'assets/images/team-management.png',
      alt: 'w-blue hr system',
      title: 'W-Blue HR System',
      description: 'Complete package also known as a Human Resources Information System (HRIS) or Human Resources Management System (HRMS), that helps organizations to manage employee records, information, accounting and applicant screening.'
    },
    {
      link: './w-blue-movie-streaming-app',
      img: 'assets/images/monitor.png',
      alt: 'w-blue movie streaming app',
      title: 'W-Blue Movie Streaming App',
      description: 'An online movie platform for movie enthusiasts competing with other online streaming apps in the market with lowest rate but equipped with the best quality.'
    },
    {
      link: './w-pay',
      img: 'assets/images/data-analysis-finance-arrow-graph-money.png',
      alt: 'w-pay',
      title: 'W-Pay',
      description: 'Financial technology services network for small and medium-size enterprises also known as financial services solutions.'
    },
    {
      link: './24-7-support',
      img: 'assets/images/open-24-hours.png',
      alt: '24/7 support',
      title: '24/7 Support',
      description: 'We have a support hotline for all the apps we made for our customers to protect them in any inconvenience.'
    },
    {
      link: './call-center-management',
      img: 'assets/images/technical-support.png',
      alt: 'call center management',
      title: 'Call Center Management',
      description: 'We create the first ever mobile call center solution. It\'s a full-scale, secure enterprise call center on mobile. Agents deliver high-quality customer service with just a smartphone and stable internet connection.'
    },
    {
      link: './tech-support',
      img: 'assets/images/tech.png',
      alt: 'tech support',
      title: 'Tech Support',
      description: 'We are a full-stack software development company. Our industry software experts have years of experience building business automation web solutions for a number of verticals. We use custom APIs and our mastery of web services to build web-based automation services for applications like inventory control, digital marketing and customer relationship management.'
    },
    {
      link: './website-design-development',
      img: 'assets/images/web-page.png',
      alt: 'web design and development',
      title: 'Web Design & Development',
      description: 'We are a full-stack software development company. Our industry software experts have years of experience building business automation web solutions for a number of verticals. We use custom APIs and our mastery of web services to build web-based automation services for applications like inventory control, digital marketing and customer relationship management.'
    },
    {
      link: './cyber-security',
      img: 'assets/images/cyber-security.png',
      alt: 'cyber security',
      title: 'Cyber Security',
      description: 'W-Blue commits to guide you throughout your digital transformation journey in Doha Qatar. Also known as Multi-Level Protection Scheme (MLPS), is a regulatory scheme designed to protect the cybersecurity of networks and systems in Doha. We aim to protect the network and the system components from interruption, damage, unauthorized access with a tiered concept to avoid any data leakage, manipulation, and eavesdropping. It is compulsory for all companies and individuals that own, operate, or provide services relating to network and corresponding system components in Doha to follow the national standards under the MLPS scheme.'
    },
  ]

}
