import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items = [
    {
      img: 'assets/images/integrity-colored.png',
      alt: 'integrity',
      title: 'Integrity',
      description: 'W-Blue adheres to strict and moral ethical codes, We put a premium on good practice, and we penalize actions that are against the law.'
    },
    {
      img: 'assets/images/excellence.png',
      alt: 'excellence',
      title: 'Excellence',
      description: 'Through continuous improvement aiming for excellence from input to the output. Our technical and professional expertise strives to excel and will stops at nothing to improve and be the best service and solution provider.'
    },
    {
      img: 'assets/images/professionalism.png',
      alt: 'professionalism',
      title: 'Professionalism',
      description: 'Values on-time delivery and dedicates to strategize absolute and progressive work environment.'
    },
    {
      img: 'assets/images/client-orientation.png',
      alt: 'client or customer orientation',
      title: 'Client or Customer Orientation',
      description: 'Knows the nature and scope of our clients to better-met or even exceed their expectations. Disappointment is not an option. We aim for our client\'s satisfaction.'
    },
    {
      img: 'assets/images/teamwork.png',
      alt: 'teamwork',
      title: 'Teamwork',
      description: 'Working in a healthy and harmonious peer-to-peer relationship. W-Blue cooperative effort works toward upholding open and honest communication between peers to achieve common goals.'
    },
    {
      img: 'assets/images/fexibility.png',
      alt: 'flexibility',
      title: 'Flexibility',
      description: 'As an Avant Garde of versatility, we are diverse in providing solutions to our clients.'
    },
    {
      img: 'assets/images/efficiency.png',
      alt: 'efficiency',
      title: 'Efficiency',
      description: 'We work within constraints which displays the high-degree of skill within the given amount of time.'
    },
    {
      img: 'assets/images/respect.png',
      alt: 'respect',
      title: 'Respect',
      description: 'Respect for ourselves, for work and authority, for other people and everything that involves life.'
    },
  ];

}
