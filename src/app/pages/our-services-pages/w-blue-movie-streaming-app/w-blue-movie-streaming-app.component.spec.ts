import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WBlueMovieStreamingAppComponent } from './w-blue-movie-streaming-app.component';

describe('WBlueMovieStreamingAppComponent', () => {
  let component: WBlueMovieStreamingAppComponent;
  let fixture: ComponentFixture<WBlueMovieStreamingAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WBlueMovieStreamingAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WBlueMovieStreamingAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
