import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WMallComponent } from './w-mall.component';

describe('WMallComponent', () => {
  let component: WMallComponent;
  let fixture: ComponentFixture<WMallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WMallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WMallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
