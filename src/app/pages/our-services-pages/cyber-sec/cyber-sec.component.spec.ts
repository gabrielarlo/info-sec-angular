import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CyberSecComponent } from './cyber-sec.component';

describe('CyberSecComponent', () => {
  let component: CyberSecComponent;
  let fixture: ComponentFixture<CyberSecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CyberSecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyberSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
