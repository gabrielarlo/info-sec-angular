import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfosecComponent } from './infosec.component';

describe('InfosecComponent', () => {
  let component: InfosecComponent;
  let fixture: ComponentFixture<InfosecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfosecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfosecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
