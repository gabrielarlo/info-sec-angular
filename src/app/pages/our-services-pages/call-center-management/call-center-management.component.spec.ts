import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallCenterManagementComponent } from './call-center-management.component';

describe('CallCenterManagementComponent', () => {
  let component: CallCenterManagementComponent;
  let fixture: ComponentFixture<CallCenterManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallCenterManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallCenterManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
