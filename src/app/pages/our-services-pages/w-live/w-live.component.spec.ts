import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WLiveComponent } from './w-live.component';

describe('WLiveComponent', () => {
  let component: WLiveComponent;
  let fixture: ComponentFixture<WLiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WLiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
