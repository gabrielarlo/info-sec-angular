import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WPayComponent } from './w-pay.component';

describe('WPayComponent', () => {
  let component: WPayComponent;
  let fixture: ComponentFixture<WPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
