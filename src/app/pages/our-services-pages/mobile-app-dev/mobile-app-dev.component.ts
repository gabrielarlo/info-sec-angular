import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mobile-app-dev',
  templateUrl: './mobile-app-dev.component.html',
  styleUrls: ['./mobile-app-dev.component.css']
})
export class MobileAppDevComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items = [
    {
      img: 'assets/images/ios.png',
      alt: 'ios app development',
      title: 'IOS App Development',
      description: 'Making the best use out of Objective-C/Swift to create top-notch iOS apps meeting the strict demands and quality standards of Apple.'
    },
    {
      img: 'assets/images/android.png',
      alt: 'android app development',
      title: 'Android App Development',
      description: 'The old-school Java codes are written with fresher perspectives to create amazing, client-centric Android apps with trendiest designs.'
    },
    {
      img: 'assets/images/laptop.png',
      alt: 'cross platform app development',
      title: 'Cross Platform App Development',
      description: 'Rather than rummaging through a list of available technologies, it is better to acquire a powerful cross-platform app solution to reach a wider audience in different industries.'
    },
    {
      img: 'assets/images/shapes.png',
      alt: '2d/3d game development',
      title: '2D/3D Game Development',
      description: 'From linear, obstacle crossing 2D games to the realistic graphics matching the detailing and crispness of a top 3D game, you get to acquire quality mobile games irrespective of the idea and scope in mind.'
    },
  ]
}
