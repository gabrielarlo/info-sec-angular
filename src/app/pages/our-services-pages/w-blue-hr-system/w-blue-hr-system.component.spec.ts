import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WBlueHrSystemComponent } from './w-blue-hr-system.component';

describe('WBlueHrSystemComponent', () => {
  let component: WBlueHrSystemComponent;
  let fixture: ComponentFixture<WBlueHrSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WBlueHrSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WBlueHrSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
