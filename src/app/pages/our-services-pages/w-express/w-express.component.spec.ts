import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WExpressComponent } from './w-express.component';

describe('WExpressComponent', () => {
  let component: WExpressComponent;
  let fixture: ComponentFixture<WExpressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WExpressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WExpressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
