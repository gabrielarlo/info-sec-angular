import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebDesignDevComponent } from './web-design-dev.component';

describe('WebDesignDevComponent', () => {
  let component: WebDesignDevComponent;
  let fixture: ComponentFixture<WebDesignDevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebDesignDevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebDesignDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
