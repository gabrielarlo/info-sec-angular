import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { CareersComponent } from './pages/careers/careers.component';
import { OurServicesComponent } from './pages/our-services/our-services.component';
import { MobileAppDevComponent } from './pages/our-services-pages/mobile-app-dev/mobile-app-dev.component';
import { WExpressComponent } from './pages/our-services-pages/w-express/w-express.component';
import { WMallComponent } from './pages/our-services-pages/w-mall/w-mall.component';
import { WLiveComponent } from './pages/our-services-pages/w-live/w-live.component';
import { InfosecComponent } from './pages/our-services-pages/infosec/infosec.component';
import { WBlueHrSystemComponent } from './pages/our-services-pages/w-blue-hr-system/w-blue-hr-system.component';
import { WBlueMovieStreamingAppComponent } from './pages/our-services-pages/w-blue-movie-streaming-app/w-blue-movie-streaming-app.component';
import { WPayComponent } from './pages/our-services-pages/w-pay/w-pay.component';
import { SupportComponent } from './pages/our-services-pages/support/support.component';
import { CallCenterManagementComponent } from './pages/our-services-pages/call-center-management/call-center-management.component';
import { TechSupportComponent } from './pages/our-services-pages/tech-support/tech-support.component';
import { WebDesignDevComponent } from './pages/our-services-pages/web-design-dev/web-design-dev.component';
import { CyberSecComponent } from './pages/our-services-pages/cyber-sec/cyber-sec.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const routes: Routes = [

  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    children: [
      {
        path: '',
        component: HomeComponent
      }
    ]
  },
  {
    path: 'about-us',
    children: [
      {
        path: '',
        component: AboutUsComponent
      }
    ]
  },
  {
    path: 'our-services',
    children: [
      {
        path: '',
        component: OurServicesComponent
      }
    ]
  },
  {
    path: 'our-services',
    children: [
      {
        path: 'mobile-application-development',
        component: MobileAppDevComponent,
      },
      {
        path: 'w-express',
        component: WExpressComponent
      },
      {
        path: 'w-mall',
        component: WMallComponent
      },
      {
        path: 'w-live',
        component: WLiveComponent
      },
      {
        path: 'information-security',
        component: InfosecComponent
      },
      {
        path: 'w-blue-hr-system',
        component: WBlueHrSystemComponent
      },
      {
        path: 'w-blue-movie-streaming-app',
        component: WBlueMovieStreamingAppComponent
      },
      {
        path: 'w-pay',
        component: WPayComponent
      },
      {
        path: '24-7-support',
        component: SupportComponent
      },
      {
        path: 'call-center-management',
        component: CallCenterManagementComponent
      },
      {
        path: 'tech-support',
        component: TechSupportComponent
      },
      {
        path: 'website-design-development',
        component: WebDesignDevComponent
      },
      {
        path: 'cyber-security',
        component: CyberSecComponent
      },
    ]
  },
  {
    path: 'contact-us',
    children: [
      {
        path: '',
        component: ContactUsComponent
      }

    ]
  },
  {
    path: 'careers',
    children: [
      {
        path: '',
        component: CareersComponent
      }

    ]
  },
  {
    path: "**",
    component: PageNotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
