import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SideNavComponent } from './layout/side-nav/side-nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { FooterComponent } from './layout/footer/footer.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome'
import { faTwitter, faFacebookF, faInstagram, faLinkedinIn, fab } from '@fortawesome/free-brands-svg-icons';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HomeComponent } from './pages/home/home.component';
import { OurPartnersComponent } from './layout/our-partners/our-partners.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { CareersComponent } from './pages/careers/careers.component';
import { OurServicesComponent } from './pages/our-services/our-services.component';
import { MobileAppDevComponent } from './pages/our-services-pages/mobile-app-dev/mobile-app-dev.component';
import { WExpressComponent } from './pages/our-services-pages/w-express/w-express.component';
import { WMallComponent } from './pages/our-services-pages/w-mall/w-mall.component';
import { WLiveComponent } from './pages/our-services-pages/w-live/w-live.component';
import { InfosecComponent } from './pages/our-services-pages/infosec/infosec.component';
import { WBlueHrSystemComponent } from './pages/our-services-pages/w-blue-hr-system/w-blue-hr-system.component';
import { WBlueMovieStreamingAppComponent } from './pages/our-services-pages/w-blue-movie-streaming-app/w-blue-movie-streaming-app.component';
import { WPayComponent } from './pages/our-services-pages/w-pay/w-pay.component';
import { SupportComponent } from './pages/our-services-pages/support/support.component';
import { CallCenterManagementComponent } from './pages/our-services-pages/call-center-management/call-center-management.component';
import { TechSupportComponent } from './pages/our-services-pages/tech-support/tech-support.component';
import { WebDesignDevComponent } from './pages/our-services-pages/web-design-dev/web-design-dev.component';
import { CyberSecComponent } from './pages/our-services-pages/cyber-sec/cyber-sec.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    ContactUsComponent,
    FooterComponent,
    HomeComponent,
    OurPartnersComponent,
    AboutUsComponent,
    CareersComponent,
    OurServicesComponent,
    MobileAppDevComponent,
    WExpressComponent,
    WMallComponent,
    WLiveComponent,
    InfosecComponent,
    WBlueHrSystemComponent,
    WBlueMovieStreamingAppComponent,
    WPayComponent,
    SupportComponent,
    CallCenterManagementComponent,
    TechSupportComponent,
    WebDesignDevComponent,
    CyberSecComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDividerModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    FontAwesomeModule,
    MatInputModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fab);
    library.addIcons(faTwitter);
    library.addIcons(faFacebookF);
    library.addIcons(faInstagram);
    library.addIcons(faLinkedinIn);
  }
}
