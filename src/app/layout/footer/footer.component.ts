import { Component, OnInit } from '@angular/core';
import { faTwitter, faFacebookF, faInstagram, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  twitter = faTwitter;
  fb = faFacebookF;
  insta = faInstagram;
  linkedin = faLinkedinIn;

  constructor() { }

  ngOnInit(): void {
  }

}
