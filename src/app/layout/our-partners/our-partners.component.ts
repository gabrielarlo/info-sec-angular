import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-our-partners',
  templateUrl: './our-partners.component.html',
  styleUrls: ['./our-partners.component.css']
})
export class OurPartnersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  partners = [
    {
      img: 'assets/images/wblue-mall.png',
      alt: 'wblue mall online marketplace',
    },
    {
      img: 'assets/images/wlive.png',
      alt: 'wlive',
    },
    {
      img: 'assets/images/wblue-x.png',
      alt: 'wblue express',
    },
    {
      img: 'assets/images/golden-octopus-head.png',
      alt: 'golden octupos head',
    },
  ]
}
