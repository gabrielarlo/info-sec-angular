import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  showToolbar: boolean;
  showHamburger: boolean;
  constructor() { }

  ngOnInit(): void {
  }

  isLargeScreen() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width > 1080) {
      this.showToolbar = true;
      this.showHamburger = false;
      return true;
    } else {
      this.showToolbar = false
      this.showHamburger = true;
      return false;
    }
  }

}
